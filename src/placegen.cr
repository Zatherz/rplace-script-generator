require "stumpy_png"
require "colorize"
require "logger"

require "./colormap"
require "./javascript"

class PlaceGen
  LOGGER = Logger.new STDERR
  LOGGER.level = Logger::WARN

  @canvas : StumpyPNG::Canvas
  @position : Tuple(Int32, Int32)
  @filename : String
  @test_mode : Bool
  @alpha_color : Int32? = nil

  def initialize(@position, input_png @filename, @test_mode = false, alpha_color : String? = nil)
    @canvas = StumpyPNG.read @filename

    if alpha_color
      hex = alpha_color.starts_with?('#') ? alpha_color : "##{alpha_color}"
      @alpha_color = find_nearest_color StumpyCore::RGBA.from_hex(hex).to_rgb8
    end

    LOGGER.debug "canvas: #{@canvas}"
  end

  def generate_colors_array : Array(Array(Int8))
    unmapped = [] of Tuple(Int32, Int32, Tuple(UInt8, UInt8, UInt8))

    output = [] of Array(Int8)

    @@color_map.each do |k, v|
      LOGGER.debug "color mapping: #{k} => #{v}"
    end

    @canvas.width.times do |x|
      output << [] of Int8
      @canvas.height.times do |y|
        color = @canvas[x, y]
        color_tup = @canvas[x, y].to_rgb8

        if color.a < 1
          if @alpha_color
            mapped = @alpha_color.not_nil!
          else
            mapped = -1
          end
        else
          mapped = find_nearest_color color_tup
        end

        LOGGER.debug "pixel: {#{x}, #{y}} color #{color.to_rgb8} mapped to #{mapped}"

        output.last << mapped.to_i8
      end
    end

    output
  end

  def to_s(str)
    str << JavascriptGenerator.new(
      filename: @filename,
      colors: generate_colors_array,
      position_x: @position[0],
      position_y: @position[1],
      test_mode: @test_mode
    ).create_script
  end
end

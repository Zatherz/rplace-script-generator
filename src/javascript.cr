class PlaceGen
  class JavascriptGenerator
    @position_x : Int32
    @position_y : Int32
    @colors : Array(Array(Int8))
    @filename : String
    @test_mode : Bool

    @@script_base = File.read(File.join(__DIR__, "base.js"))

    def initialize(@filename, @colors, @position_x, @position_y, @test_mode = false)
    end

    def generate_colors_array
      String.build do |str|
        str.puts "["
        @colors.each_with_index do |ary, j|
          str << "  ["
          ary.each_with_index do |e, i|
            str << e.to_s.rjust(2, ' ')
            str << ", " if i != ary.size - 1
          end
          str << "]"
          str << "," if j != @colors.size - 1
          str << "\n"
        end
        str.puts "]"
      end
    end

    def create_script
      @@script_base
        .gsub("%%SUB%%COLORS%%", generate_colors_array)
        .gsub("%%SUB%%XPOS%%", @position_x)
        .gsub("%%SUB%%YPOS%%", @position_y)
        .gsub("%%SUB%%FILENAME%%", @filename)
        .gsub("%%SUB%%TESTMODEBOOL%%", @test_mode)
    end
  end
end

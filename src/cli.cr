require "option_parser"
require "colorize"
require "http"

require "./placegen"

class PlaceGen
  def self.cli
    output_file = STDOUT
    debug_enabled = false
    test_mode = false
    alpha_color = nil

    pos_x = nil
    pos_y = nil

    parser = OptionParser.parse! do |parser|
      parser.banner = "Usage: placegen file.png -x X_POSITION -y Y_POSITION [--alphacolor=FFFFFF] [-t/--test] [-o/--output -] [-d/--debug]"
      parser.on "-o OUT", "--output=OUT", "Set the output (- for stdout, + for stderr)" do |arg|
        case arg
        when "-" then output_file = STDOUT
        when "+" then output_file = STDERR
        else          output_file = File.open arg, "w"
        end
      end
      parser.on "-h", "--help", "Show help" do
        puts parser
        exit
      end
      parser.on "-d", "--debug", "Enable debug output" do
        debug_enabled = true
      end
      parser.on "-x X_POSITION", "--posx=X_POSITION", "X position of the top left corner" do |arg|
        pos_x = arg.to_i32
      end
      parser.on "-y Y_POSITION", "--posy=Y_POSITION", "Y position of the top left corner" do |arg|
        pos_y = arg.to_i32
      end
      parser.on "-t", "--test", "Enable test mode (image will be drawn instantly, but only client side)" do
        test_mode = true
      end
      parser.on "-a COL", "--alphacolor=COL", "Set the background color (replacement color for transparent pixels)" do |arg|
        alpha_color = arg
      end
    end

    LOGGER.level = Logger::DEBUG if debug_enabled

    if ARGV.size < 1
      STDERR.puts parser
      exit 1
    end

    input_file = ARGV[0]

    if !File.exists? input_file
      LOGGER.error "File #{input_file} doesn't exist"
      STDERR.puts parser
      exit 1
    end

    if pos_x.nil? || pos_y.nil?
      LOGGER.error "Must provide position"
      STDERR.puts parser
      exit 1
    end

    output_file.puts PlaceGen.new(
      position: {pos_x.not_nil!, pos_y.not_nil!},
      input_png: input_file,
      test_mode: test_mode,
      alpha_color: alpha_color
    )

    output_file.close # dispose!
  end
end

PlaceGen.cli

require "stumpy_png"

class PlaceGen
  class_getter color_map = {
    {255, 255, 255} => 0,
    {228, 228, 228} => 1,
    {136, 136, 136} => 2,
    {34, 34, 34}    => 3,
    {255, 167, 209} => 4,

    {229, 0, 0}    => 5,
    {229, 149, 0}  => 6,
    {160, 106, 66} => 7,
    {229, 217, 0}  => 8,
    {148, 224, 68} => 9,

    {2, 190, 1}     => 10,
    {0, 211, 221}   => 11,
    {0, 131, 199}   => 12,
    {0, 0, 234}     => 13,
    {207, 110, 228} => 14,
    {130, 0, 128}   => 15,
  }

  def find_nearest_color(tup : Tuple(UInt8, UInt8, UInt8))
    # optimization for perfect matches
    if _color = @@color_map[tup]?
      return _color
    end

    scores = {} of Int32 => Int32
    @@color_map.each do |k, v|
      dist = (k[0] - tup[0]).abs + (k[1] - tup[1]).abs + (k[2] - tup[2]).abs
      scores[dist] = v
      LOGGER.debug "distance: #{dist} for #{v}"
    end

    scores[scores.keys.min]
  end
end

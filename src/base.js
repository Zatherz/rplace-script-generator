var colors = %%SUB%%COLORS%%

var colorsABGR = [];
 
var position = {
  x: %%SUB%%XPOS%%,
  y: %%SUB%%YPOS%%,
};
 
var placed = 0;
 
// hooks
var client;
var canvasse;
var jQuery;
 
var test = %%SUB%%TESTMODEBOOL%%;
 
r.placeModule("drawing", function(e){
  client = e("client");
  canvasse = e("canvasse");
  jQuery = e("jQuery");
 
  for(var i=0; i<client.palette.length; i++){
    colorsABGR[i] = client.getPaletteColorABGR(i);
  }
 
  if (test) {
    drawTest();
  } else {
    draw();
  }

  console.log("=====================================================")
  console.log("auto-/r/place script originally made by MaximumSalmon")
  console.log("edited by Zatherz")
  console.log("drawing %%SUB%%FILENAME%%")
  console.log("at (%%SUB%%XPOS%%,%%SUB%%YPOS%%)")
  console.log("run drawTest() to draw the full image (client side!)")
  console.log("=====================================================")
});
 
function draw(){
  var toWait = client.getCooldownTimeRemaining();
  if(toWait === 0) {
    var terminate_loop = false;
    for(var x = 0; x < colors.length; x++) {
        for (var y = 0; y < colors[x].length; y++) {
          var targetx = position.x + x;
          var targety = position.y + y;

          var pixelColor = getPixel(targetx, targety);

          var color = colorsABGR[colors[x][y]];
          if(pixelColor !== color && colors[x][y] > -1){
            console.log("Drawing color " + colors[x][y] + " at position {" + targetx + "," + targety + "}")
            client.setColor(colors[x][y]);
            client.drawTile(
              position.x + x,
              position.y + y
            );
            terminate_loop = true;
          }
          if (terminate_loop) {break;}
        }
        if (terminate_loop) {break;}
     }
  }
  setTimeout(draw, toWait + Math.round(Math.random() * 1500));
}
 
function drawTest(){
  for(var x = 0; x < colors.length; x++){
    for(var y = 0; y < colors[x].length; y++) {
      if (colors[x][y] > -1) {
        canvasse.drawTileAt(
          position.x + x,
          position.y + y,
          colorsABGR[colors[x][y]]
        );
      }
    }
  }
}
 
function getPixel(x, y){
  return canvasse.writeBuffer[canvasse.getIndexFromCoords(x, y)];
}

all: placegen

placegen: src/placegen.cr src/log.cr src/javascript.cr src/colormap.cr src/cli.cr src/base.js
	mkdir -p bin/
	crystal build --release src/cli.cr -o bin/placegen

clean:
	rm -rf bin/

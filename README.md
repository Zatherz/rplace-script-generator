# PLACE HAS ENDED

/r/place has ended. This script will now only function in the test/clientside mode (with the `-t`/`--test` option).  

# PlaceGen

PlaceGen is a small program that generates a JS script intended to be activated on the /r/place page.  
It takes a PNG image and X,Y coordinates and automatically places missing pixels for you whenever possible.  

NOTE: This does not work around the timer. It will still only place a pixel once every 5, 10 or 20 minutes depending on what the timer is set to.

## Building

`make placegen`

## Usage

`bin/placegen --help`

## Activating the output script

On Firefox or Chrome, press F12 (or right click -> Inspect [Element]), select the Console tab, paste the output of this program, and then press Enter.

## Contributors

- [Zatherz](https://gitlab.com/u/Zatherz) - creator, maintainer
